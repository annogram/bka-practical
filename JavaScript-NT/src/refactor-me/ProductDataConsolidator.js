﻿/**
 * Consolidates data from mock database.
 * @constructor 
 */

function ProductDataConsolidator() {
	this.lawnmowers = new LawnmowerRepository().getAll();
	this.phoneCaseRepository = new PhoneCaseRepository().getAll();
	this.tShirtRepository = new TShirtRepository().getAll();
	this.US = 0.76;
	this.Euros = 0.67;
	this.NZD = 1.00;
}

/**
 * Converts product values into the correct currency depending on string input,
 * if no string is given NZD is used by default.
 * 
 * @returns {object[]} of product objects in the correct currency
 * @param {string} currency
 */
ProductDataConsolidator.prototype.get = function (currency) {

	var products = [];
	var multiplier = this.NZD;
	if(currency != NaN){
		multiplier = (currency === "USD") ? this.US 
			: (currency === "EURO") ? this.Euros : this.NZD;
	}

	for (var i = 0; i < this.lawnmowers.length; i++) {
		products.push({
			id: this.lawnmowers[i].id,
			name: this.lawnmowers[i].name,
			price: (this.lawnmowers[i].price * multiplier).toFixed(2),
			type: "Lawnmower"
		});
	}

	for (var i = 0; i < this.phoneCaseRepository.length; i++) {
		products.push({
			id: this.phoneCaseRepository[i].id,
			name: this.phoneCaseRepository[i].name,
			price: (this.phoneCaseRepository[i].price * multiplier).toFixed(2),
			type: "Phone Case"
		});
	}

	for (var i = 0; i < this.tShirtRepository.length; i++) {
		products.push({
			id: this.tShirtRepository[i].id,
			name: this.tShirtRepository[i].name,
			price: (this.tShirtRepository[i].price * multiplier).toFixed(2),
			type: "T-Shirt"
		});
	}

	return products;
}