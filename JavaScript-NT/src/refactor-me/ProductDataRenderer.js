﻿/**
 * Renders data pulled from the consolidator onto the HTML page.
 * @constructor
 */
function ProductDataRenderer() {
	this.pdc = new ProductDataConsolidator();

	/**
	 * Creates an HTML table.
	 * @param {string} title
	 * @param {Object[]} tableContents
	 * @returns {string} table 
	 */
	this.renderTable =  (title, tableContents) => {
		var table = '<table class="table table-striped">'
			+'<thead>'
				+'<tr><td colspan="3">'+title+'</td></tr>'
				+'<tr>'
					+'<td>Name</td>'
					+'<td>Price</td>'
					+'<td>Type</td>'
				+'</tr>'
			+'</thead>';
		tableContents.forEach(function(value, index, arr){
			table += '<tr>'
			+		'<td>' + value.name +'</td>'
			+		'<td>' + value.price + '</td>'
			+		'<td>' + value.type + '</td>'
			+	'</tr>';
		}, table);
		table += '</tbody></table>'
		return table;
	}
}

/**
 * Render a table inside a given Div element.
 * @param {string} Location
 * @param {string} Currency
 * @param {string} TableHeader
 */
ProductDataRenderer.prototype.render = function (Location, Currency, TableHeader) {
	var data = this.pdc.get(Currency);
	var table = this.renderTable(TableHeader, data);
	document.getElementById(Location).innerHTML = table;
}