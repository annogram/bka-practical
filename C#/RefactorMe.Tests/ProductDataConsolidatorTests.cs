﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RefactorMe.Consolidators.Enumerables;
using RefactorMe.Consolidators;
using FakeItEasy;
using RefactorMe.DontRefactor.Models;
using System.Collections.Generic;
using RefactorMe.Consolidators.Implementation;
using RefactorMe.DontRefactor.Data;
using RefactorMe.DontRefactor.Data.Implementation;

namespace RefactorMe.Tests
{
    [TestClass]
    public class ProductDataConsolidatorTests
    {
        private ProductDataConsolidator pdc;
        private List<Product> expectedProducts;

        [TestInitialize]
        public void Initialize() {
            pdc = new ProductDataConsolidator() {
                ExchangeRate = Currency.NZD
            };
            Guid lid = Guid.NewGuid();
            Guid pid = Guid.NewGuid();
            Guid tid = Guid.NewGuid();
             expectedProducts = new List<Product> {
                new Product() {
                    Id = lid,
                    Name = "Hewlett-Packard Rideable Lawnmower",
                    Price = 3000.0,
                    Type = ProductEnum.Lawnmower.GetValue()
                },
                new Product() {
                    Id = pid,
                    Name = "Amazon Fire Burgundy Phone Case",
                    Price = 14.0,
                    Type = ProductEnum.PhoneCase.GetValue()
                },
                new Product() {
                    Id = tid,
                    Name = "Xamarin C# T-Shirt",
                    Price = 15.0,
                    Type = ProductEnum.TShirt.GetValue()
                }
            };
        }

        [TestMethod]
        public void CurrencyEnum_EnumTest_ValuesReturnProperly() {
            // Arrange
            var NZD = Currency.NZD;
            var USD = Currency.USD;
            var EUR = Currency.EUR;

            // Act /Assert
            Assert.AreEqual<double>(1.00, NZD.ConversionRate());
            Assert.AreEqual<double>(0.76, USD.ConversionRate());
            Assert.AreEqual<double>(0.67, EUR.ConversionRate());
        }

        [TestMethod]
        public void ProductDataConsolidator_MethodTest_GetValuesFromAddedRepository() {
            // Arrange
            pdc.lawnmower = new LawnmowerRepository();
            string expectedValue = "Hewlett-Packard Rideable Lawnmower";
            // Act
            IEnumerable<string> actualValue = pdc.Get(ProductEnum.Lawnmower).Select(s => s.Name);
            //Assert
            Assert.IsTrue(actualValue.Contains(expectedValue));
        }

        [TestMethod]
        public void ProductDataConsolidator_MethodTest_GetAllReturnsAllExistingProducts() {
            // Arrange
            pdc.lawnmower = new LawnmowerRepository();
            pdc.phonecase = new PhoneCaseRepository();
            pdc.tshirt = new TShirtRepository();
            // Act
            List<Product> actualValues = pdc.GetAll();
            // Assert
            foreach (var p in expectedProducts) {
                Assert.IsNotNull(expectedProducts
                    .Select(s => s.Name)
                    .Where(s => s == p.Name)
                    .FirstOrDefault());
            }
        }
    }
}
