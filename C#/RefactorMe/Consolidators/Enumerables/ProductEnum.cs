﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefactorMe.Consolidators.Enumerables
{
    public enum ProductEnum { Lawnmower, PhoneCase, TShirt }

    public static class ProductEnumExtensions
    {
        public static string GetValue(this ProductEnum productType) {
            switch (productType) {
                case ProductEnum.Lawnmower:
                    return "Lawnmower";
                case ProductEnum.PhoneCase:
                    return "Phone Case";
                case ProductEnum.TShirt:
                    return "T-Shirt";
                default:
                    return null;
            }
        }
    }
}
