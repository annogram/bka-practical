﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefactorMe.Consolidators.Enumerables {
    /// <summary>
    /// This enumeration is modifiable and new currency values can be added if the 
    /// need arrises.
    /// 
    /// When adding new enums the values should be the conversion rate * 100.
    /// </summary>
    public enum Currency {NZD, USD, EUR}


    public static class CurrencyExtensions {
        /// <summary>
        /// Simple Enumeration extension to get the currency value from the 
        /// corresponding enum.
        /// </summary>
        /// <param name="val">This enum</param>
        /// <returns>Conversion rate</returns>
        public static double ConversionRate(this Currency val) {
            switch (val) {
                case Currency.USD:
                    return 0.76;
                case Currency.EUR:
                    return 0.67;
                case Currency.NZD:
                default:
                    return 1.00;
            }
        }
    }
}
