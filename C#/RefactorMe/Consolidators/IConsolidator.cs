﻿using RefactorMe.DontRefactor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RefactorMe.Consolidators.Enumerables;
using RefactorMe.DontRefactor.Data;

namespace RefactorMe.Consolidators
{
    /// <summary>
    /// Interface for defining protocols to get <see cref="Product"/>s
    /// </summary>
    /// <typeparam name="TItemEnum">Use this to define different repository types</typeparam>
    public interface IConsolidator<TItemEnum>
    {
        // Property so this can object can be instansiated by dependency injection.
        Currency ExchangeRate { get; set; }

        List<Product> GetAll();
        List<Product> Get(TItemEnum productType);
    }
}
