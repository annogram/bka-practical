﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RefactorMe.Consolidators.Enumerables;
using RefactorMe.DontRefactor.Models;
using RefactorMe.DontRefactor.Data;
using RefactorMe.DontRefactor.Data.Implementation;

namespace RefactorMe.Consolidators.Implementation
{
    /// <summary>
    /// Consolidates product data from product repositories.
    /// </summary>
    public class ProductDataConsolidator : BaseConsolidator<ProductEnum>
    {
        public LawnmowerRepository lawnmower { get; set; }
        public PhoneCaseRepository phonecase { get; set; }
        public TShirtRepository tshirt { get; set; }

        public override List<Product> GetAll() {
            var output = new List<Product>();
            foreach (var i in lawnmower.GetAll()) {
                output.Add(new Product() {
                    Id = i.Id,
                    Name = i.Name,
                    Price = i.Price,
                    Type = "Lawnmower"
                });
            }

            foreach (var i in phonecase.GetAll()) {
                output.Add(new Product() {
                    Id = i.Id,
                    Name = i.Name,
                    Price = i.Price,
                    Type = "Phone Case"
                });
            }

            foreach (var i in tshirt.GetAll()) {
                output.Add(new Product() {
                    Id = i.Id,
                    Name = i.Name,
                    Price = i.Price,
                    Type = "T-Shirt"
                });
            }
            return output;
        }

        public override List<Product> Get(ProductEnum productType) {
            var output = new List<Product>();
            switch (productType) {
                case ProductEnum.Lawnmower:
                    foreach (var i in lawnmower.GetAll()) {
                        output.Add(new Product() {
                            Id = i.Id,
                            Name = i.Name,
                            Price = i.Price,
                            Type = productType.GetValue()
                        });
                    }
                    break;
                case ProductEnum.PhoneCase:
                    foreach (var i in phonecase.GetAll()) {
                        output.Add(new Product() {
                            Id = i.Id,
                            Name = i.Name,
                            Price = i.Price,
                            Type = productType.GetValue()
                        });
                    }
                    break;
                case ProductEnum.TShirt:
                    foreach (var i in tshirt.GetAll()) {
                        output.Add(new Product() {
                            Id = i.Id,
                            Name = i.Name,
                            Price = i.Price,
                            Type = productType.GetValue()
                        });
                    }
                    break;
            }
            return output;
        }
    }
}
