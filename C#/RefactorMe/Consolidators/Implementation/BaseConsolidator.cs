﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RefactorMe.Consolidators.Enumerables;
using RefactorMe.DontRefactor.Data;
using RefactorMe.DontRefactor.Models;
using RefactorMe.DontRefactor.Data.Implementation;

namespace RefactorMe.Consolidators.Implementation
{
    public abstract class BaseConsolidator<TItemEnum> : IConsolidator<TItemEnum>
    {
        public Currency ExchangeRate { get; set; }
        public abstract List<Product> GetAll();
        public abstract List<Product> Get(TItemEnum productType);

    }
}